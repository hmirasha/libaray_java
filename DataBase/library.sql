-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2018 at 08:38 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `library`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `secquestion` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`username`, `name`, `password`, `secquestion`, `answer`) VALUES
('ras', 'rasha', '123', 'Where are you from?', 'syria'),
('ro', 'rola', '111', 'Where are you from?', 'syria'),
('rol', 'rola', '222', 'What\'s your birthday?', '1994'),
('roll', 'rola', '777', 'Where are you from?', 'syria'),
('rrr', 'rasha', '333', 'Whats your nickname?', 'syria'),
('rub', 'ruba', '222', 'Whats your nickname?', 'hmi');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `bookid` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `edition` int(10) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  `pages` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`bookid`, `name`, `edition`, `publisher`, `price`, `pages`) VALUES
(1, 'aaa', 2, '1999', 200, 33),
(66, 'hkhjkj', 3, '33', 22, 11),
(70, 'jjj', 1, '788', 32, 444),
(178, 'jjjj', 3, '87', 44, 87),
(314, 'll', 1, '87', 11, 23),
(437, 'ttt', 1, '77', 34, 87),
(439, 'kkk', 1, '98', 76, 34),
(548, 'bbb', 2, '88', 87, 11),
(582, 'yyy', 1, '87', 332, 122),
(647, 'hhkjh', 1, '3322', 22, 33),
(710, 'tt', 1, '99', 66, 55);

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE `issue` (
  `bookid` int(10) NOT NULL,
  `bookname` varchar(255) NOT NULL,
  `edition` int(10) NOT NULL,
  `publisher` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `pages` int(10) NOT NULL,
  `studentid` int(10) NOT NULL,
  `studentname` varchar(255) NOT NULL,
  `fathername` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `year` int(10) NOT NULL,
  `semester` int(10) NOT NULL,
  `dateofissue` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue`
--

INSERT INTO `issue` (`bookid`, `bookname`, `edition`, `publisher`, `price`, `pages`, `studentid`, `studentname`, `fathername`, `branch`, `year`, `semester`, `dateofissue`) VALUES
(66, 'hkhjkj', 3, 33, 22, 11, 98, 'jj', 'hhk', 'ioi', 1, 1, '3/4/2000'),
(70, 'jjj', 1, 788, 32, 444, 147, 'mmm', 'dcc', 'aaa', 3, 2, '5/7/2015'),
(178, 'jjjj', 3, 87, 44, 87, 539, 'oooo', 'nnn', 'jjhhh', 1, 1, '3/2/2005'),
(437, 'ttt', 1, 77, 34, 87, 991, 'rrr', 'jjjj', 'hkhklhkj', 3, 2, '3/4/2011');

-- --------------------------------------------------------

--
-- Table structure for table `returnbook`
--

CREATE TABLE `returnbook` (
  `studentid` int(10) NOT NULL,
  `studentname` varchar(255) NOT NULL,
  `fathername` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `year` int(10) NOT NULL,
  `semester` int(10) NOT NULL,
  `bookid` int(10) NOT NULL,
  `bookname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `edition` int(10) NOT NULL,
  `publisher` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `pages` int(10) NOT NULL,
  `dateofissue` varchar(255) NOT NULL,
  `dateofreturn` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `returnbook`
--

INSERT INTO `returnbook` (`studentid`, `studentname`, `fathername`, `branch`, `year`, `semester`, `bookid`, `bookname`, `edition`, `publisher`, `price`, `pages`, `dateofissue`, `dateofreturn`) VALUES
(98, 'jj', 'hhk', 'ioi', 1, 1, 66, 'hkhjkj', 3, 33, 22, 11, '3/4/2000', '3/4/2001'),
(539, 'oooo', 'nnn', 'jjhhh', 1, 1, 178, 'jjjj', 3, 87, 44, 87, '3/2/2005', '4/4/2009');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `studentid` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fathername` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `year` int(10) NOT NULL,
  `semester` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`studentid`, `name`, `fathername`, `branch`, `year`, `semester`) VALUES
(98, 'jj', 'hhk', 'ioi', 1, 1),
(147, 'mmm', 'dcc', 'aaa', 3, 2),
(539, 'oooo', 'nnn', 'jjhhh', 1, 1),
(991, 'rrr', 'jjjj', 'hkhklhkj', 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`bookid`);

--
-- Indexes for table `issue`
--
ALTER TABLE `issue`
  ADD PRIMARY KEY (`bookid`);

--
-- Indexes for table `returnbook`
--
ALTER TABLE `returnbook`
  ADD PRIMARY KEY (`studentid`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`studentid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
